package be.heh.amauryd.android_project.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import be.heh.amauryd.android_project.Database.User;
import be.heh.amauryd.android_project.Database.UserAccessDB;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.SessionManager;
import be.heh.amauryd.android_project.Utilities.PasswordHasher;

public class AdminActivity extends AppCompatActivity {

    private UserAccessDB userAccessDB;
    private SessionManager sessionManager;
    private ListView list_users;
    private CheckBox check_read_write;
    private Button button_change_readWrite , button_change_password;
    private EditText input_admin_user_mail , input_newPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        userAccessDB = new UserAccessDB(this);
        sessionManager = new SessionManager(this);
        userAccessDB.openForWrite();

        list_users = findViewById(R.id.list_view);
        button_change_readWrite = findViewById(R.id.button_change_readWrite);
        input_admin_user_mail = findViewById(R.id.input_admin_user_mail);
        check_read_write = findViewById(R.id.check_read_write);
        input_newPassword = findViewById(R.id.input_newPassword);
        button_change_password = findViewById(R.id.button_change_password);

        ArrayList<User> tab_user = userAccessDB.getAllUser();
        ArrayList<String> user_mails = new ArrayList<>();

        for (User user : tab_user) {
            user_mails.add(user.getMail());
        }

        list_users.setAdapter(new ArrayAdapter<>(this,R.layout.activity_list_view,user_mails));

        button_change_readWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = input_admin_user_mail.getText().toString();
                Boolean canRead = check_read_write.isChecked();

                User user = userAccessDB.getUser(email);
                user.setAction(canRead);

                if (user != null)
                {
                    if (user.getAuthority().equals("admin"))
                    {
                        Toast.makeText(AdminActivity.this, "Cannot change rights of admin", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    userAccessDB.updateUser(user.getId(),user);
                }
            }
        });
        button_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassword =  input_newPassword.getText().toString();

                if (newPassword.length() < 4)
                {
                    Toast.makeText(AdminActivity.this, "Password length must be over 4 chars", Toast.LENGTH_SHORT).show();
                    return;
                }

                User user = userAccessDB.getUser(sessionManager.getUserEmail());

                if (user != null)
                {
                    user.setPassword(PasswordHasher.hash_sha256(newPassword));
                    userAccessDB.updateUser(user.getId(),user);
                    sessionManager.storeUser(user);
                    Toast.makeText(AdminActivity.this, "Password changed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
