package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import be.heh.amauryd.android_project.Database.User;
import be.heh.amauryd.android_project.Database.UserAccessDB;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.SessionManager;
import be.heh.amauryd.android_project.Utilities.PasswordHasher;

public class RegisterActivity extends AppCompatActivity {
    private UserAccessDB userDB;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        userDB = new UserAccessDB(this);
        userDB.openForWrite();

        sessionManager = new SessionManager(this);
    }

    public void onRegisterClick(View view)
    {
        EditText firstName = findViewById(R.id.input_firstName);
        EditText name = findViewById(R.id.input_name);
        EditText password = findViewById(R.id.input_password);
        EditText email = findViewById(R.id.input_email);

        if (firstName.getText().toString().isEmpty() ||
                name.getText().toString().isEmpty() ||
                email.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Fields must not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.getText().toString().length() < 4)
        {
            Toast.makeText(this, "Password length must be more than 4 characters", Toast.LENGTH_SHORT).show();
            return;
        }

        User user = userDB.getUser(email.getText().toString());

        if (user == null) {
            User newUser = new User(
                    -1,
                    name.getText().toString(),
                    firstName.getText().toString(),
                    email.getText().toString(),
                    PasswordHasher.hash_sha256(password.getText().toString()),
                    false
            );
            long id = userDB.insertUser(newUser);
            newUser.setId((int)id);
            sessionManager.storeUser(newUser);
            sessionManager.loginUser();
            Toast.makeText(this, "Successfully registered", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show();
        }
    }
}
