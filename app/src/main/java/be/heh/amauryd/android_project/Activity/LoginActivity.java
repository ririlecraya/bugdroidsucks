package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import be.heh.amauryd.android_project.Database.UserAccessDB;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.SessionManager;
import be.heh.amauryd.android_project.Utilities.PasswordHasher;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private UserAccessDB userDB;
    private SessionManager manager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        manager = new SessionManager(this);
        userDB = new UserAccessDB(this);

        userDB.openForWrite();
    }

    public void onLoginClick(View view) {
        EditText password = findViewById(R.id.input_password);
        EditText email = findViewById(R.id.input_email);

        boolean exists = userDB.checkUser(email.getText().toString(),PasswordHasher.hash_sha256(password.getText().toString()));

        if (exists) {
            Intent i = new Intent(this, MainActivity.class);
            manager.storeUser(userDB.getUser(email.getText().toString()));

            if (!manager.loginUser())
                Log.i("PATATE","Failed to login user");

            this.startActivity(i);
            finish();
        }else{
            Toast.makeText(this, "Wrong username or password", Toast.LENGTH_SHORT).show();
        }
    }
}

