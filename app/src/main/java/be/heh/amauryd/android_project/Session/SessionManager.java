package be.heh.amauryd.android_project.Session;

import android.content.Context;
import android.content.SharedPreferences;

import be.heh.amauryd.android_project.Database.User;

public class SessionManager {
    // Shared Preferences variable
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SessionManager(Context context)
    {
        preferences= context.getSharedPreferences("preferences",Context.MODE_PRIVATE);
        editor= preferences.edit();
    }

    // function store user details
    public boolean storeUser(User user)
    {
        editor.putString("firstName",user.getFistname());
        editor.putString("name",user.getName());
        editor.putString("password",user.getPassword());
        editor.putString("email",user.getMail());
        editor.putString("authority",user.getAuthority());
        editor.putBoolean("writeLevel",user.getWriteLevel());

        return editor.commit();
    }

    // to login user
    public boolean loginUser()
    {
        editor.putBoolean("is_log",true);
        return editor.commit();
    }

    public Boolean getWriteLevel() { return preferences.getBoolean("writeLevel",false); }

    public String getAuthotity() { return preferences.getString("authority",""); }

    //to get username
    public String getUserName()
    {
        return preferences.getString("firstName","");
    }

    //to get userpassword
    public String getUserPassword()
    {
        return preferences.getString("password","");
    }

    //to get useremail
    public String getUserEmail()
    {
        return preferences.getString("email","");
    }

    //to check whether user is login or not
    public boolean isUserLogedIn()
    {
        return preferences.getBoolean("is_log",false);
    }

    // to delete the user and clear the preferences
    public boolean logOutUser()
    {
        editor.clear();
        return editor.commit();
    }

}