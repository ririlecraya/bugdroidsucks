package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import be.heh.amauryd.android_project.MachineInteraction.ReadTaskS7Comprimes;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.MachinePreferences;
import be.heh.amauryd.android_project.Session.SessionManager;

public class ComprimeActivity extends AppCompatActivity {

    private ReadTaskS7Comprimes readTaskS7Comprimes;
    private MachinePreferences preferences;
    private SessionManager sessionManager;

    private Button button_open_write_panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprime);

        button_open_write_panel = findViewById(R.id.button_open_write_panel);

        readTaskS7Comprimes = new ReadTaskS7Comprimes(findViewById(R.id.layout_global_comprime));
        preferences = new MachinePreferences(this);
        sessionManager = new SessionManager(this);

        String comprime_ip = preferences.preferences.getString("comprime_ip","192.168.137.2");
        Integer comprime_rack = preferences.preferences.getInt("comprime_rack",0);
        Integer comprime_slot = preferences.preferences.getInt("comprime_slot",2);

        readTaskS7Comprimes.start(comprime_ip, String.valueOf(comprime_rack), String.valueOf(comprime_slot));

        button_open_write_panel.setEnabled(sessionManager.getWriteLevel());

        if (sessionManager.getWriteLevel()) {
            button_open_write_panel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(),ComprimeWriteActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
