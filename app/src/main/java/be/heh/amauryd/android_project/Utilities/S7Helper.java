package be.heh.amauryd.android_project.Utilities;

import be.heh.amauryd.android_project.S7.S7;
import be.heh.amauryd.android_project.S7.S7Client;

public class S7Helper {
    public static Boolean canConnect(String ip,Integer rack,Integer slot)
    {
        S7Client client = new S7Client();
        client.SetConnectionType(S7.S7_BASIC);
        int res = client.ConnectTo(ip, rack, slot);

        return res == 0;
    }
}
