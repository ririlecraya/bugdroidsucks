package be.heh.amauryd.android_project.MachineInteraction;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.S7.S7;
import be.heh.amauryd.android_project.S7.S7Client;
import be.heh.amauryd.android_project.S7.S7OrderCode;

public class ReadTaskS7Level {
    private static final int MESSAGE_PRE_EXECUTE = 1;
    private static final int MESSAGE_PROGRESS_UPDATE = 2;
    private static final int MESSAGE_POST_EXECUTE = 3;
    private static final int MESSAGE_ERROR = 4;

    private AtomicBoolean isRunning = new AtomicBoolean(false);

    private TextView text_model , text_liquidlevel , text_consigneManu , text_consigneAuto , text_van;
    private Switch switch_isWorking;

    private class MessageData {
        private HashMap<String,Object> datas = new HashMap<>();
        public Object getElement(String key) { return datas.get(key); }
        public void setElement(String key,Object value) { datas.put(key,value); }
    }

    private AutomateS7 plcS7;
    private Thread readThread;
    private S7Client comS7;
    private String[] param = new String[12];
    private byte[] datasPLC = new byte[512];

    private LinearLayout layout;

    public ReadTaskS7Level(View view) {
        text_model = view.findViewById(R.id.text_level_model);
        text_liquidlevel = view.findViewById(R.id.text_liquidlevel);
        text_consigneAuto = view.findViewById(R.id.text_consigneAuto);
        text_consigneManu = view.findViewById(R.id.text_consigneManu);
        text_van = view.findViewById(R.id.text_van);
        switch_isWorking = view.findViewById(R.id.switch_isWorking);

        layout = (LinearLayout)view;

        comS7 = new S7Client();
        plcS7 = new AutomateS7();
        readThread = new Thread(plcS7);
    }

    @SuppressLint("HandlerLeak")
    private Handler monHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_PRE_EXECUTE:
                    downloadOnPreExecute((String)msg.obj);
                    break;
                case MESSAGE_PROGRESS_UPDATE:
                    updateView((MessageData)msg.obj);
                    break;
                case MESSAGE_POST_EXECUTE:
                    downloadOnPostExecute();
                    break;
                case MESSAGE_ERROR:
                    renderErrorMessage((String)msg.obj);
                default:
                    break;
            }
        }
    };

    public void stop()
    {
        isRunning.set(false);
        comS7.Disconnect();
        readThread.interrupt();
    }

    public void start(String address,String rack,String slot)
    {
        if (!readThread.isAlive())
        {
            param[0] = address;
            param[1] = rack;
            param[2] = slot;
        }

        readThread.start();
        isRunning.set(true);
    }

    private void downloadOnPreExecute(String machineName)
    {
        text_model.setText("Model : " + machineName);
    }

    private void renderErrorMessage(String message)
    {
        layout.setVisibility(View.INVISIBLE);
        Toast.makeText(layout.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void updateView(MessageData data) {
        int liquidLevel = (int)data.getElement("liquidLevel");
        int autoConsign = (int)data.getElement("autoConsign");
        int manuConsign = (int)data.getElement("manuConsign");
        int pilotWord = (int)data.getElement("pilotWord");
        boolean isWorking = (boolean)data.getElement("isWorking");

        switch_isWorking.setChecked(isWorking);
        text_liquidlevel.setText("Liquid level : " + liquidLevel);
        text_consigneAuto.setText("Consigne auto : " + autoConsign);
        text_consigneManu.setText("Consigne manual : " + manuConsign);
        text_van.setText("Pilot word :" + pilotWord);
    }

    @SuppressLint("SetTextI18n" )
    private void downloadOnPostExecute() {

    }

    private void sendPostExecuteMessage() {
        Message postExecuteMsg = new Message();
        postExecuteMsg.what = MESSAGE_POST_EXECUTE;
        monHandler.sendMessage(postExecuteMsg);
    }

    private void sendErrorMessage(String message) {
        Message postExecuteMsg = new Message();
        postExecuteMsg.what = MESSAGE_ERROR;
        postExecuteMsg.obj = message;
        monHandler.sendMessage(postExecuteMsg);
    }

    private void sendPreExecuteMessage(String m) {
        Message preExecuteMsg = new Message();
        preExecuteMsg.what = MESSAGE_PRE_EXECUTE;
        preExecuteMsg.obj = m;
        monHandler.sendMessage(preExecuteMsg);
    }

    private void sendUpdateMessage(MessageData data) {
        Message progressMsg = new Message();
        progressMsg.what = MESSAGE_PROGRESS_UPDATE;
        progressMsg.obj = data;
        monHandler.sendMessage(progressMsg);
    }

    private class AutomateS7 implements Runnable {
        private Integer readShort(int data_block_number,int amount)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            if (retInfo == 0)
                return S7.GetShortAt(dataBloc, 0);
            else
                return -1;
        }

        private Boolean readBit(int data_block_number,int amount,int position)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            boolean bit = S7.GetBitAt(dataBloc , 0 ,position);

            return bit;
        }

        private Integer readWord(int data_block_number,int amount)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            if (retInfo == 0)
               return S7.GetWordAt(dataBloc, 0);
            else
               return -1;
        }

        @Override
        public void run() {
            try {
                comS7.SetConnectionType(S7.S7_BASIC);
                Integer res = comS7.ConnectTo(param[0], Integer.valueOf(param[1]), Integer.valueOf(param[2]));
                S7OrderCode orderCode = new S7OrderCode();
                Integer result = comS7.GetOrderCode(orderCode);

                if (res.equals(0) && !result.equals(0)) {
                    sendErrorMessage("Can't connect to application");
                    return;
                }

                sendPreExecuteMessage(orderCode.Code());

                while (isRunning.get()) {
                    MessageData data = new MessageData();

                    data.setElement("isWorking",readBit(0,2,5));
                    data.setElement("liquidLevel",readWord(16,2));
                    data.setElement("autoConsign",readWord(18,2));
                    data.setElement("manuConsign",readWord(20,2));
                    data.setElement("pilotWord",readWord(22,2));

                    sendUpdateMessage(data);
                    Thread.sleep(1000);
                }
                sendPostExecuteMessage();
            }catch (Exception e)
            {
                sendErrorMessage(e.getMessage());
            }
        }
    }
}
