package be.heh.amauryd.android_project.MachineInteraction;

import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import be.heh.amauryd.android_project.S7.S7;
import be.heh.amauryd.android_project.S7.S7Client;

/*
    Opérations d’écriture de 8 valeurs(BOOL) dans
    la Variable suivante :
    1. DB5.DBB7
    Opérations d’écriture d’un BYTE (8 bits)  dans la
    Variable suivante :
    1. DB5.DBB8
    Opérations d’écriture de 16 valeurs(BOOL)
    dans les Variables suivantes :
    1. DB5.DBB5
    2. DB5.DBB6
    Opérations d’écriture dans une variable INTEGER
    (16 bits) :
    1. DB5.DBW18
 */

public class WriteTaskS7 {
    private AtomicBoolean isRunning = new AtomicBoolean(false);
    private Thread writeThread;
    private AutomateS7 plcS7;
    private S7Client comS7;
    private String[] parConnexion = new String[10];

    private ArrayBlockingQueue<Command> queue;

    public WriteTaskS7(){
        queue = new ArrayBlockingQueue<>(100);

        comS7 = new S7Client();
        plcS7 = new AutomateS7();
        writeThread = new Thread(plcS7);
    }
    public void Stop(){
        isRunning.set(false);
        comS7.Disconnect();
        writeThread.interrupt();
    }
    public void Start(String a, String r, String s){
        if (!writeThread.isAlive()) {
            parConnexion[0] = a;
            parConnexion[1] = r;
            parConnexion[2] = s;
            writeThread.start();
            isRunning.set(true);
        }
    }

    public void addTask(Command command)
    {
        queue.add(command);
    }

    private Integer writeByte(int data_block_number,int amount,int value)
    {
        byte[] dataBloc = new byte[2];

        if (value > 255)
            value = 255;

        char array[] = Integer.toBinaryString(value).toCharArray();

        for (int i = 0; i < array.length; i++) {
            char current = array[i];
            S7.SetBitAt(dataBloc,0, i,current == '1');
        }

        return comS7.WriteArea(S7.S7AreaDB, 5, data_block_number, 1 , dataBloc);
    }

    private Integer writeBit(int data_block_number,int amount,int position,boolean value)
    {
        byte[] dataBloc = new byte[2];
        int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

        S7.SetBitAt(dataBloc,0 ,position,value);
        return comS7.WriteArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);
    }

    private Integer writeWord(int data_block_number,int amount,int value)
    {
        byte[] dataBloc = new byte[2];
        int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

        S7.SetWordAt(dataBloc, 0,value);
        return comS7.WriteArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);
    }

    private class AutomateS7 implements Runnable {
        @Override
        public void run() {
            try {
                comS7.SetConnectionType(S7.S7_BASIC);
                comS7.ConnectTo(parConnexion[0], Integer.valueOf(parConnexion[1]), Integer.valueOf(parConnexion[2]));

                while (isRunning.get()) {
                    Command command = queue.take();

                    Log.i("PATATE",command.toString());

                    switch (command.command_type) {
                        case WRITE_BYTE:
                            writeByte(command.block_number, command.amount, command.value);
                            break;
                        case WRITE_WORD:
                            writeWord(command.block_number, command.amount, command.value);
                            break;
                        case WRITE_BIT:
                            writeBit(command.block_number, command.amount, command.position, command.value == 1);
                            break;
                    }
                }
            }catch (Exception e) {
                run();
            }
        }
    }
}
