package be.heh.amauryd.android_project.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ArrayList;

public class UserAccessDB {
    private SQLiteDatabase db;
    private UserDBSqlite userdb;

    public UserAccessDB(Context c){
        userdb = new UserDBSqlite(c, UserTableDefines.TABLE_NAME, null, UserTableDefines.VERSION);
    }

    public ArrayList<User> getAllUser() {
        Cursor c = db.query(UserTableDefines.TABLE_NAME,
                new String[] {
                        UserTableDefines.COL_ID,
                        UserTableDefines.COL_NAME,
                        UserTableDefines.COL_FIRSTNAME ,
                        UserTableDefines.COL_MAIL ,
                        UserTableDefines.COL_PASSWORD,
                        UserTableDefines.COL_AUTHORITY ,
                        UserTableDefines.COL_ACTION
                },
                null,
                null,
                null,
                null,
                UserTableDefines.COL_NAME
        );
        ArrayList<User> tabUser = new ArrayList<User> ();

        if (c.getCount() == 0) {
            c.close();
            return tabUser;
        }

        while (c.moveToNext())
        {
            User user1 = new User();

            user1.setId(c.getInt(UserTableDefines.NUM_COL_ID));
            user1.setFirstName(c.getString(UserTableDefines.NUM_COL_FIRSTNAME));
            user1.setName(c.getString(UserTableDefines.NUM_COL_NAME));
            user1.setMail(c.getString(UserTableDefines.NUM_COL_MAIL));
            user1.setPassword(c.getString(UserTableDefines.NUM_COL_PASSWORD));
            user1.setAuthority(c.getString(UserTableDefines.NUM_COL_AUTHORITY));
            user1.setAction(c.getInt(UserTableDefines.NUM_COL_ACTION) == 1);

            tabUser.add(user1);
        }

        c.close();
        return tabUser;
    }

    public void openForWrite(){
        db = userdb.getWritableDatabase();
    }

    public void openForRead(){
        db = userdb.getReadableDatabase();
    }

    public void close(){
        db.close();
    }

    public User getUser(String email) {
        String selection = UserTableDefines.COL_MAIL + " = ?";

        // selection arguments
        String[] selectionArgs = {email};

        Cursor cursor = db.query(UserTableDefines.TABLE_NAME, //Table to query
                new String[] {UserTableDefines.COL_ID, UserTableDefines.COL_NAME, UserTableDefines.COL_FIRSTNAME , UserTableDefines.COL_MAIL , UserTableDefines.COL_PASSWORD, UserTableDefines.COL_AUTHORITY , UserTableDefines.COL_ACTION},                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        return cursorToUser(cursor);
    }

    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                UserTableDefines.COL_ID
        };
        // selection criteria
        String selection = UserTableDefines.COL_MAIL + " = ?" + " AND " + UserTableDefines.COL_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        Cursor cursor = db.query(UserTableDefines.TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();

        return cursorCount > 0;

    }

    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                UserTableDefines.COL_ID
        };
        // selection criteria
        String selection = UserTableDefines.COL_MAIL + " = ?";

        // selection arguments
        String[] selectionArgs = {email};

        Cursor cursor = db.query(UserTableDefines.TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();

        return cursorCount > 0;
    }

    public User cursorToUser(Cursor c) {
        if (c.getCount() == 0) {
            c.close();
            return null;
        }
        c.moveToFirst();
        User user1 = new User();
        user1.setId(c.getInt(UserTableDefines.NUM_COL_ID));
        user1.setFirstName(c.getString(UserTableDefines.NUM_COL_FIRSTNAME));
        user1.setName(c.getString(UserTableDefines.NUM_COL_NAME));
        user1.setMail(c.getString(UserTableDefines.NUM_COL_MAIL));
        user1.setPassword(c.getString(UserTableDefines.NUM_COL_PASSWORD));
        user1.setAuthority(c.getString(UserTableDefines.NUM_COL_AUTHORITY));
        user1.setAction(c.getInt(UserTableDefines.NUM_COL_ACTION) == 1);
        c.close();
        return user1;
    }

    public long insertUser(User user)
    {
        ContentValues content = new ContentValues();
        content.put(UserTableDefines.COL_FIRSTNAME, user.getFistname());
        content.put(UserTableDefines.COL_NAME, user.getName());
        content.put(UserTableDefines.COL_PASSWORD, user.getPassword());
        content.put(UserTableDefines.COL_MAIL, user.getMail());
        return db.insert(UserTableDefines.TABLE_NAME, null, content);
    }

    public int updateUser(int i,User user)
    {
        ContentValues content = new ContentValues();
        content.put(UserTableDefines.COL_FIRSTNAME, user.getFistname());
        content.put(UserTableDefines.COL_NAME, user.getName());
        content.put(UserTableDefines.COL_PASSWORD, user.getPassword());
        content.put(UserTableDefines.COL_MAIL, user.getMail());
        content.put(UserTableDefines.COL_ACTION, user.getWriteLevel());
        Log.i("patate updateUser",content.toString());

        return db.update(UserTableDefines.TABLE_NAME,content, UserTableDefines.COL_ID + " = " + i,null);
    }

    public int removeUser(String login) {
        return db.delete(UserTableDefines.TABLE_NAME, UserTableDefines.COL_MAIL + " = " + login, null);
    }

    public int truncateDB() {
        return db.delete(UserTableDefines.TABLE_NAME, null, null);
    }
}
