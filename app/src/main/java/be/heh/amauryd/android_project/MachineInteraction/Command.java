package be.heh.amauryd.android_project.MachineInteraction;

public class Command {
    public int amount;
    public int block_number;
    public Integer value;
    public Command_type command_type;
    public Integer position = null;

    public Command(int amount, int block_number, int value, Command_type command_type) {
        this.amount = amount;
        this.block_number = block_number;
        this.value = value;
        this.command_type = command_type;
    }

    public Command(int amount, int block_number, int value, Command_type command_type, int position) {
        this.amount = amount;
        this.block_number = block_number;
        this.value = value;
        this.command_type = command_type;
        this.position = position;
    }

    @Override
    public String toString() {
        return "Command{" +
                "amount=" + amount +
                ", block_number=" + block_number +
                ", value=" + value +
                ", command_type=" + command_type +
                ", position=" + position +
                '}';
    }

    public enum Command_type {
        WRITE_BYTE,
        WRITE_WORD,
        WRITE_BIT
    }
}