package be.heh.amauryd.android_project.Database;

public class User {
    private int id;
    private String name;
    private String firstname;
    private String mail;
    private String password;

    public String getFirstname() {
        return firstname;
    }

    public boolean getWriteLevel() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    private Boolean action;

    public String getAuthority() {
        return authority;
    }

    private String authority;

    public User()
    {
        this.authority = "user";
        this.id = -1;
    }

    public User(int id, String name, String firstname, String mail, String password , Boolean action) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.mail = mail;
        this.password = password;
        this.action = action;
    }

    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    public String getName() {
        return this.name;
    }

    public String getFistname() {
        return this.firstname;
    }

    public String getMail() {
        return this.mail;
    }

    public String getPassword() {
        return this.password;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {
        this.firstname = firstName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", mail='" + mail + '\'' +
                ", password='" + password + '\'' +
                ", action=" + action +
                ", authority='" + authority + '\'' +
                '}';
    }
}
