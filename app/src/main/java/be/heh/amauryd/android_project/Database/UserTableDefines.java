package be.heh.amauryd.android_project.Database;

public final class UserTableDefines {
    public static final String TABLE_NAME = "Users";
    public static final int VERSION = 1;

    public static final String COL_ID = "ID";
    public static final int NUM_COL_ID = 0;

    public static final String COL_NAME = "NAME";
    public static final int NUM_COL_NAME = 1;

    public static final String COL_FIRSTNAME = "FIRSTNAME";
    public static final int NUM_COL_FIRSTNAME = 2;

    public static final String COL_MAIL = "MAIL";
    public static final int NUM_COL_MAIL = 3;

    public static final String COL_PASSWORD = "PASSWORD";
    public static final int NUM_COL_PASSWORD = 4;

    public static final String COL_AUTHORITY = "AUTHORITY";
    public static final int NUM_COL_AUTHORITY = 5;

    public static final String COL_ACTION = "ACTION_LEVEL";
    public static final int NUM_COL_ACTION = 6;

    public static final String DB_NAME = TABLE_NAME + ".db";
}
