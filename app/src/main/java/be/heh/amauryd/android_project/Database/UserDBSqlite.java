package be.heh.amauryd.android_project.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import be.heh.amauryd.android_project.Utilities.PasswordHasher;

public class UserDBSqlite extends SQLiteOpenHelper {

    public UserDBSqlite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String create = "CREATE TABLE IF NOT EXISTS " + UserTableDefines.TABLE_NAME + " (" +
                UserTableDefines.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                UserTableDefines.COL_NAME +" TEXT NOT NULL," +
                UserTableDefines.COL_FIRSTNAME  +" TEXT NOT NULL," +
                UserTableDefines.COL_MAIL +" TEXT NOT NULL," +
                UserTableDefines.COL_PASSWORD +" TEXT NOT NULL," +
                UserTableDefines.COL_AUTHORITY + " TEXT NOT NULL DEFAULT 'user'," +
                UserTableDefines.COL_ACTION + " INTEGER NOT NULL DEFAULT 0" +
                ")";

        db.execSQL(create);

        String passwordHashed = PasswordHasher.hash_sha256("test123*");
        String execString = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s) VALUES ('%s','%s','%s','%s','%s','%s')",
                UserTableDefines.TABLE_NAME,UserTableDefines.COL_NAME,UserTableDefines.COL_FIRSTNAME,UserTableDefines.COL_MAIL,UserTableDefines.COL_PASSWORD,UserTableDefines.COL_AUTHORITY,UserTableDefines.COL_ACTION,
                "root","root","root",passwordHashed,"admin",1
        );

        db.execSQL(execString);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + UserTableDefines.TABLE_NAME);
        onCreate(db);
    }
}
