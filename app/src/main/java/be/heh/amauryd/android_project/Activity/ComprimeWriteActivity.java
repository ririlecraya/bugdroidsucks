package be.heh.amauryd.android_project.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import be.heh.amauryd.android_project.MachineInteraction.Command;
import be.heh.amauryd.android_project.MachineInteraction.WriteTaskS7;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.MachinePreferences;

public class ComprimeWriteActivity extends AppCompatActivity {

    private Button button_write_bit , button_write_byte ,button_write_integer;
    private WriteTaskS7 writeTaskS7;
    private MachinePreferences preferences;
    private Spinner spinner;
    private EditText text_bit_pos , text_byte_value , text_integer_value;
    private CheckBox bitVal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprime_write);

        button_write_bit = findViewById(R.id.button_write_bit);
        button_write_byte = findViewById(R.id.button_write_byte);
        button_write_integer = findViewById(R.id.button_write_integer);

        text_bit_pos = findViewById(R.id.text_bit_pos);
        text_byte_value = findViewById(R.id.text_byte_value);
        text_integer_value = findViewById(R.id.text_integer_value);

        bitVal = findViewById(R.id.check_bit_val);

        spinner = findViewById(R.id.spinner_datablock);
        
        writeTaskS7 = new WriteTaskS7();
        preferences = new MachinePreferences(this);

        String comprime_ip = preferences.preferences.getString("comprime_ip","192.168.137.2");
        Integer comprime_rack = preferences.preferences.getInt("comprime_rack",0);
        Integer comprime_slot = preferences.preferences.getInt("comprime_slot",2);

        List<String> categories = new ArrayList<>();
        for (int i = 0; i < 32; i++) {
            categories.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        writeTaskS7.Start(comprime_ip,String.valueOf(comprime_rack),String.valueOf(comprime_slot));

        button_write_bit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Integer bit_pos = Integer.valueOf(text_bit_pos.getText().toString());
                    Integer id = (int)spinner.getSelectedItemId();
                    Integer bit = bitVal.isChecked() ? 1 : 0;

                    Command command = new Command(2,
                            id,
                            bit,
                            Command.Command_type.WRITE_BIT,
                            bit_pos
                    );

                    writeTaskS7.addTask(command);
                }catch (Exception e)
                {
                    Toast.makeText(ComprimeWriteActivity.this, "An error occured , please check your inputs", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button_write_byte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Integer id = (int) spinner.getSelectedItemId();
                    Integer value = Integer.valueOf(text_byte_value.getText().toString());

                    Command command = new Command(2,
                            id, value, Command.Command_type.WRITE_BYTE
                    );

                    writeTaskS7.addTask(command);
                }catch (Exception e)
                {
                    Toast.makeText(ComprimeWriteActivity.this, "An error occured , please check your inputs", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button_write_integer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Integer id = (int)spinner.getSelectedItemId();
                    Integer value = Integer.valueOf(text_integer_value.getText().toString());

                    Command command = new Command(2,
                            id, value, Command.Command_type.WRITE_WORD
                    );

                    writeTaskS7.addTask(command);
                }catch (Exception e)
                {
                    Toast.makeText(ComprimeWriteActivity.this, "An error occured , please check your inputs", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
