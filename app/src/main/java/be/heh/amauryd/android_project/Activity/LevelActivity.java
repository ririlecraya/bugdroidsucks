package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import be.heh.amauryd.android_project.MachineInteraction.ReadTaskS7Level;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.MachinePreferences;
import be.heh.amauryd.android_project.Session.SessionManager;

public class LevelActivity extends AppCompatActivity {

    private ReadTaskS7Level readTaskS7Level;
    private MachinePreferences preferences;
    private SessionManager sessionManager;

    private Button button_open_write_panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        readTaskS7Level = new ReadTaskS7Level(findViewById(R.id.layout_global_level));
        preferences = new MachinePreferences(this);

        sessionManager = new SessionManager(this);
        button_open_write_panel = findViewById(R.id.button_open_write_level);


        String level_ip = preferences.preferences.getString("level_ip", "192.168.137.2");
        Integer level_rack = preferences.preferences.getInt("level_rack", 0);
        Integer level_slot = preferences.preferences.getInt("level_slot", 2);

        readTaskS7Level.start(level_ip, String.valueOf(level_rack), String.valueOf(level_slot));

        button_open_write_panel.setEnabled(sessionManager.getWriteLevel());

        if (sessionManager.getWriteLevel()) {
            button_open_write_panel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), LevelActivityWriteActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
