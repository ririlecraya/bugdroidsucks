package be.heh.amauryd.android_project.MachineInteraction;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.S7.S7;
import be.heh.amauryd.android_project.S7.S7Client;
import be.heh.amauryd.android_project.S7.S7OrderCode;

public class ReadTaskS7Comprimes {
    private static final int MESSAGE_PRE_EXECUTE = 1;
    private static final int MESSAGE_PROGRESS_UPDATE = 2;
    private static final int MESSAGE_POST_EXECUTE = 3;
    private static final int MESSAGE_ERROR = 4;
    private AtomicBoolean isRunning = new AtomicBoolean(false);

    private class MessageData {
        private HashMap<String,Object> datas = new HashMap<>();
        public Object getElement(String key) { return datas.get(key); }
        public void setElement(String key,Object value) { datas.put(key,value); }
    }

    private AutomateS7 plcS7;
    private Thread readThread;
    private S7Client comS7;
    private String[] param = new String[12];

    private TextView text_title , text_model , text_pills , text_bottles , text_pillsNumberPerBottle;
    private LinearLayout layout;
    private Switch switch_isWorking;

    public ReadTaskS7Comprimes(View view) {

        text_title = view.findViewById(R.id.text_title);
        text_model = view.findViewById(R.id.text_model);
        text_pills =  view.findViewById(R.id.text_currenPillsNumberPerBottle);
        text_bottles = view.findViewById(R.id.text_productedBottles);
        switch_isWorking = view.findViewById(R.id.switch_isWorking);
        text_pillsNumberPerBottle = view.findViewById(R.id.text_pillsNumberPerBottle);

        layout = (LinearLayout)view;

        comS7 = new S7Client();
        plcS7 = new AutomateS7();
        readThread = new Thread(plcS7);
    }

    @SuppressLint("HandlerLeak")
    private Handler monHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_PRE_EXECUTE:
                    downloadOnPreExecute((String)msg.obj);
                    break;
                case MESSAGE_PROGRESS_UPDATE:
                    updateView((MessageData)msg.obj);
                    break;
                case MESSAGE_POST_EXECUTE:
                    downloadOnPostExecute();
                    break;
                case MESSAGE_ERROR:
                    renderErrorMessage((String)msg.obj);
                default:
                    break;
            }
        }
    };

    public void stop()
    {
        isRunning.set(false);
        comS7.Disconnect();
        readThread.interrupt();
    }

    public void start(String address,String rack,String slot)
    {
        if (!readThread.isAlive())
        {
            param[0] = address;
            param[1] = rack;
            param[2] = slot;
        }

        readThread.start();
        isRunning.set(true);
    }

    private void downloadOnPreExecute(String machineName)
    {
        text_model.setText("Model : " + machineName);
    }

    private void renderErrorMessage(String message)
    {
        layout.setVisibility(View.INVISIBLE);
        Toast.makeText(layout.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void updateView(MessageData data) {
        int totalBottles = (int)data.getElement("totalBottles");
        int pillsNumber = (int)data.getElement("pillsNumber");
        boolean isWorking = (boolean)data.getElement("isWorking");
        int pillsPerBottle = (int)data.getElement("pillsPerBottle");

        String pillsText = "Pills per bottle : ";

        if (pillsPerBottle == 0)
            pillsText += "5";
        else if (pillsPerBottle == 1)
            pillsText += "10";
        else
            pillsText += "15";


        text_bottles.setText("Total bottles : " + String.valueOf(totalBottles));
        text_pills.setText("Current pills : " + String.valueOf(pillsNumber));
        text_pillsNumberPerBottle.setText(pillsText);
        switch_isWorking.setChecked(isWorking);
    }

    @SuppressLint("SetTextI18n" )
    private void downloadOnPostExecute() {

    }

    private void sendPostExecuteMessage() {
        Message postExecuteMsg = new Message();
        postExecuteMsg.what = MESSAGE_POST_EXECUTE;
        monHandler.sendMessage(postExecuteMsg);
    }

    private void sendErrorMessage(String message) {
        Message postExecuteMsg = new Message();
        postExecuteMsg.what = MESSAGE_ERROR;
        postExecuteMsg.obj = message;
        monHandler.sendMessage(postExecuteMsg);
    }

    private void sendPreExecuteMessage(String m) {
        Message preExecuteMsg = new Message();
        preExecuteMsg.what = MESSAGE_PRE_EXECUTE;
        preExecuteMsg.obj = m;
        monHandler.sendMessage(preExecuteMsg);
    }

    private void sendUpdateMessage(MessageData data) {
        Message progressMsg = new Message();
        progressMsg.what = MESSAGE_PROGRESS_UPDATE;
        progressMsg.obj = data;
        monHandler.sendMessage(progressMsg);
    }

    private class AutomateS7 implements Runnable {
        private Integer readShort(int data_block_number,int amount)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            if (retInfo == 0)
                return S7.GetShortAt(dataBloc, 0);
            else
                return -1;
        }

        private Boolean readBit(int data_block_number,int amount,int position)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            boolean bit = S7.GetBitAt(dataBloc , 0 ,position);

            return bit;
        }

        private Integer readWord(int data_block_number,int amount)
        {
            byte[] dataBloc = new byte[2];
            int retInfo = comS7.ReadArea(S7.S7AreaDB, 5, data_block_number, amount , dataBloc);

            if (retInfo == 0)
               return S7.GetWordAt(dataBloc, 0);
            else
               return -1;
        }

        @Override
        public void run() {
            try {
                comS7.SetConnectionType(S7.S7_BASIC);
                Integer res = comS7.ConnectTo(param[0], Integer.valueOf(param[1]), Integer.valueOf(param[2]));
                S7OrderCode orderCode = new S7OrderCode();
                Integer result = comS7.GetOrderCode(orderCode);

                int numCPU = -1;
                if (res.equals(0) && result.equals(0)) {
                    numCPU = Integer.valueOf(orderCode.Code().substring(5, 8));
                } else {
                    numCPU = 0000;
                }

                if (res.equals(0) && !result.equals(0)) {
                    sendErrorMessage("Can't connect to application");
                    return;
                }

                sendPreExecuteMessage(orderCode.Code());

                while (isRunning.get()) {
                    MessageData data = new MessageData();
                    data.setElement("totalBottles", readWord(16, 2));
                    data.setElement("pillsNumber", readWord(14, 2));
                    data.setElement("isWorking", readBit(0, 2, 0));

                    int bit1 = readBit(4, 2, 4) ? 1 : 0;
                    int bit2 = readBit(4, 2, 5) ? 1 : 0;

                    data.setElement("pillsPerBottle", Integer.parseInt(String.valueOf(bit2) + String.valueOf(bit1), 2));

                    sendUpdateMessage(data);

                    Thread.sleep(1000);
                }
                sendPostExecuteMessage();
            }catch (Exception e)
            {
                sendErrorMessage(e.getMessage());
            }
        }
    }
}
