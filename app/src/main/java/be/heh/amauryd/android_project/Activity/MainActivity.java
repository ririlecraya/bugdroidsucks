package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import be.heh.amauryd.android_project.Database.User;
import be.heh.amauryd.android_project.Database.UserAccessDB;
import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.MachinePreferences;
import be.heh.amauryd.android_project.Session.SessionManager;
import be.heh.amauryd.android_project.Utilities.S7Helper;

public class MainActivity extends AppCompatActivity {
    private SessionManager sessionManager;
    private MachinePreferences machinePreferences;
    private Button button_login, button_register , button_level , button_comprimes;
    private LinearLayout layout_login_register , layout_access_s7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(this);
        machinePreferences = new MachinePreferences(this);

        layout_login_register = findViewById(R.id.layout_login_register);
        layout_access_s7 = findViewById(R.id.layout_access_s7);

        button_login = findViewById(R.id.button_login);
        button_register = findViewById(R.id.button_register);
        button_level = findViewById(R.id.button_level);
        button_comprimes = findViewById(R.id.button_comprimes);

        if(sessionManager.isUserLogedIn()) {
            layout_login_register.setVisibility(View.GONE);
            layout_access_s7.setVisibility(View.VISIBLE);
        }else {
            layout_login_register.setVisibility(View.VISIBLE);
            layout_access_s7.setVisibility(View.GONE);
        }

        assignEventhandlers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (sessionManager.isUserLogedIn()) {
            menu.add(0, 0, 0, "Logout").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
               @Override
               public boolean onMenuItemClick(MenuItem item) {
                   finish();
                   startActivity(getIntent());
                   sessionManager.logOutUser();
                   return false;
               }
            });
            menu.add(0, 1, 1, "Settings").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Intent intent = new Intent(getApplicationContext(),SettingsActivity.class);
                    startActivity(intent);
                    return false;
                }
            });

            if (sessionManager.getAuthotity().equals("admin")) {
                menu.add(0, 2, 2, "Administration").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(getApplicationContext(), AdminActivity.class);
                        startActivity(intent);
                        return false;
                    }
                });
            }
        }else
            menu.removeItem(0);

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return true;
    }

    private void assignEventhandlers()
    {
        button_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent login = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(login);
                }
            }
        );
        button_register.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent login = new Intent(getApplicationContext(),RegisterActivity.class);
                   startActivity(login);
               }
           }
        );
        button_comprimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent comprimeIntent = new Intent(getApplicationContext(),ComprimeActivity.class);
                startActivity(comprimeIntent);
            }
        });

        button_level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent comprimeIntent = new Intent(getApplicationContext(),LevelActivity.class);
                startActivity(comprimeIntent);
            }
        });
    }
}
