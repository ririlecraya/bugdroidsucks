package be.heh.amauryd.android_project.Utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHasher {
    public static String hash(String hash,String text) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(hash);
        } catch (NoSuchAlgorithmException e) {
            return text;
        }
        md.update(text.getBytes());

        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public static String hash_sha256(String text) {
        return hash("SHA-256",text);
    }
}
