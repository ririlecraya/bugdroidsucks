package be.heh.amauryd.android_project.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import be.heh.amauryd.android_project.R;
import be.heh.amauryd.android_project.Session.MachinePreferences;

public class SettingsActivity extends AppCompatActivity {

    private EditText input_comprime_ip , input_comprime_rack , input_comprime_slot ,
            input_level_ip , input_level_rack , input_level_slot;
    private Button input_settings_save;
    MachinePreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        preferences = new MachinePreferences(getApplicationContext());

        input_comprime_ip = findViewById(R.id.input_comprime_ip);
        input_comprime_rack = findViewById(R.id.input_comprime_rack);
        input_comprime_slot = findViewById(R.id.input_comprime_slot);

        input_level_ip = findViewById(R.id.input_level_ip);
        input_level_rack = findViewById(R.id.input_level_rack);
        input_level_slot = findViewById(R.id.input_level_slot);

        input_settings_save = findViewById(R.id.input_settings_exit);

        String comprime_ip = preferences.preferences.getString("level_ip","192.168.137.2");
        Integer comprime_rack = preferences.preferences.getInt("level_rack",0);
        Integer comprime_slot = preferences.preferences.getInt("level_slot",2);

        String level_ip = preferences.preferences.getString("comprime_ip","192.168.137.2");
        Integer level_rack = preferences.preferences.getInt("comprime_rack",0);
        Integer level_slot = preferences.preferences.getInt("comprime_slot",2);

        input_comprime_ip.setText(comprime_ip);
        input_comprime_rack.setText(String.valueOf(comprime_rack));
        input_comprime_slot.setText(String.valueOf(comprime_slot));

        input_level_ip.setText(level_ip);
        input_level_rack.setText(String.valueOf(level_rack));
        input_level_slot.setText(String.valueOf(level_slot));
                
        assignEventhandlers();
    }

    private void assignEventhandlers()
    {
        input_settings_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(),MainActivity.class);

                String comprime_ip = input_comprime_ip.getText().toString();
                Integer comprime_rack = Integer.valueOf(input_comprime_rack.getText().toString());
                Integer comprime_slot = Integer.valueOf(input_comprime_slot.getText().toString());

                String level_ip = input_level_ip.getText().toString();
                Integer level_rack = Integer.valueOf(input_comprime_rack.getText().toString());
                Integer level_slot = Integer.valueOf(input_comprime_slot.getText().toString());

                preferences.editor.putString("level_ip",level_ip);
                preferences.editor.putInt("level_rack",level_rack);
                preferences.editor.putInt("level_slot",level_slot);

                preferences.editor.putString("comprime_ip",comprime_ip);
                preferences.editor.putInt("comprime_rack",comprime_rack);
                preferences.editor.putInt("comprime_slot",comprime_slot);

                preferences.editor.commit();

                Toast.makeText(SettingsActivity.this, "Saved Settings !", Toast.LENGTH_SHORT).show();

                startActivity(main);
                finish();
            }
        });
    }
}
