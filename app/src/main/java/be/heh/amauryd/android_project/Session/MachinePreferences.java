package be.heh.amauryd.android_project.Session;

import android.content.Context;
import android.content.SharedPreferences;

public class MachinePreferences {
    // Shared Preferences variable
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    public MachinePreferences(Context context)
    {
        preferences= context.getSharedPreferences("machine_preference",Context.MODE_PRIVATE);
        editor= preferences.edit();
    }
}
